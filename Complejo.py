
from math import sqrt


class Complejo: #Creamos la clase número complejo
    
    def __init__(self,r,i): #Le asignamos unos atributos al número. Tiene que tener una parte real y una parte imaginaria
        self.real = r
        self.imaginaria = i
    
    def sumame(self,n2): #Para sumar números complejos lo hacemos 1 a 1
        self.real = self.real + n2.real
        self.imaginaria = self.imaginaria + n2.imaginaria
        
    def restame (self, n2):
        self.real = self.real - n2.real
        self.imaginaria = self.imaginaria - n2.imaginaria
        
    def multiplícame(self,n2):
        tmp = Complejo(self.real,self.imaginaria)
        self.real = (self.real*n2.real) + ((-1)*self.imaginaria*n2.imaginaria)
        self.imaginaria = tmp.real*n2.imaginaria + tmp.imaginaria*n2.real
        
    def divídeme(self,n2): #Método que divide el primer número por un segundo número
        conjugado = Complejo(n2.real,-(n2.imaginaria))
        n2.multiplícame(conjugado)
        self.multiplícame(conjugado)
        self.real = self.real/n2.real
        self.imaginaria = self.imaginaria/n2.real

    @staticmethod
    def módulo(self):
        módulo = round(((self.real**2) + (self.imaginaria**2))**(1/2),3)
        return módulo
    
    def opuesto(self):
        self.real = self.real * (-1)
        self.imaginaria = self.imaginaria * (-1)
    
    def __str__(self):
        cadena = 'El resultado es: {real} + ({compleja})i'.format(real = self.real, compleja=self.imaginaria)
        return cadena




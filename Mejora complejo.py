class Complejo: #Creamos la clase número complejo
    
    def __init__(self,r,i): #Le asignamos unos atributos al número. Tiene que tener una parte real y una parte imaginaria
        self.real = r
        self.imaginaria = i
    @staticmethod
    def suma(n1,n2): #Esto es una función
            parte_real = n1.real + n2.real
            parte_imaginaria = n1.imaginaria + n2.imaginaria
            resultado = Complejo(parte_real, parte_imaginaria)
            return resultado 

    def suma_me(self, n2): #Esto es un método, no hace falta devolver algo. Este método le pertenece a 'self'
        self.real = self.real + n2.real
        self.imaginaria = self.imaginaria + n2.imaginaria

    def suma_me2(self, n2):
        self.real = self.real + n2.real
        self.imaginaria = self.imaginaria + n2.imaginaria
        return self

    def __str__(self):
            cadena = 'El resultado es: {real} + ({compleja})i'.format(real = self.real, compleja=self.imaginaria)
            return cadena

v1 = Complejo(1,1)
v2 = Complejo(2,2)
v3 = Complejo(3,3)

print(v1.suma_me2(v2).suma_me2(v3)) #Esto es lo mismo que esto
v3 = Complejo.suma(v1,v2) #Esto es una llamada a una función. La función no es de un self

v1.suma_me(v2) #Si imprimimos esto por pantalla, no te deculeve nada, pero el objeto que aloja v1 se está modificando tal que si imprimimos v1
#Nos sale como resultado la suma

Este proyecto es un ejercicio de clase que sirve para practicar lo que son las clases, los métodos, las instancias/objetos, las referencias, los constructores, etc. Todo aquello que esté relacionado con la programación orientada a objetos en python. 
El fichero 'unittests.py' contiene los test unitarios para la clase 'Complejos'
El fichero 'Complejos.py' contiene toda la información relacionada con la clase, tanto los constructores (__init__; __str__) como los métodos (dado que el programa varía el primer número para realizar la cuenta). 
El fichero 'Mejora Complejo.py' contiene apuntes y mejoras de clase aportadas por el profesor. 

Entre los métodos de la clase 'Complejos' se encuentran: un método para la suma, para la resta, para la división y para el producto. 

El objetivo de este proyecto es practicar los conocimientos dados en clase de forma que las habilidades del alumno en programación orientada a objetos mejore. 

import unittest
from Complejo import Complejo

class TestComplejo(unittest.TestCase):

    def test_constructor(self):
        n1 = Complejo(1,2)
        self.assertEqual(n1.real,1)
        self.assertEqual(n1.imaginaria,2)

    def test_suma(self):
        n1 = Complejo(1,1) #Hay alguna forma de evitar tneer que estar declarando las variables por cada método
        n2 = Complejo(2,2)

        n1.sumame(n2)
        self.assertEqual(n1.real,3)
        self.assertEqual(n1.imaginaria,3)
    
    def test_resta(self):
        n1 = Complejo(1,1)
        n2 = Complejo(2,2)

        n1.restame(n2)
        self.assertEqual(n1.real,-1)
        self.assertEqual(n1.imaginaria,-1)

    def test_multiplicación(self):

        n1 = Complejo(1,1)
        n2 = Complejo(2,2)

        n1.multiplícame(n2)
        self.assertEqual(n1.real,0)
        self.assertEqual(n1.imaginaria,4)

    def test_división(self):

        n1 = Complejo(1,1)
        n2 = Complejo(2,2)

        n1.divídeme(n2)
        self.assertEqual(n1.real,0.5)
        self.assertEqual(n1.imaginaria,0)
    
    def test_opuesto(self):

        n1 = Complejo(1,1)
        n1.opuesto()
        self.assertEqual (n1.real, -1)
        self.assertEqual (n1.imaginaria, -1)
    
    def test_módulo(self):
        
        n1 = Complejo(1,1)
        self.assertEqual(Complejo.módulo(n1), round(2**(1/2),3))

        
    def test_str(self):
        n1 = Complejo(2,-2)
        self.assertEqual('El resultado es: 2 + (-2)i',n1.__str__())


if __name__ == "__main__":
    unittest.main()
